var app = require('express')();
var server  = require('http').createServer(app);
var io = require('socket.io').listen(server);
server.listen(2014);
users = [];
io.sockets.on('connection', function(socket){
  //console.log('a user connected');

   socket.on('message', function (data) {
	 socket.broadcast.emit('message', data);
   });
	
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('chatmessage', function(msg){
	  console.log("---------chat message----------");
	socket.broadcast.to(msg.roomname).emit("chatmessage",{message: msg.message, username: msg.user});
  });

  socket.on('addsocket', function(userdetails) {
	  users.push({user: userdetails.user,socket_id: socket.id});
	  console.log(userdetails.user);
	  socket.broadcast.emit('newuser', userdetails.user);
  });

  socket.on('join room', function (room){
		socket.set("room",room, function() {console.log("room "+room+" saved");});
		socket.join(room);
	});

  socket.on('inviteVideoCall', function(msg) {
	  console.log(msg);
	  for(i=0; i<users.length; i++) {
		  console.log("----------video invite--------");
		  console.log(users[i].user);
		  console.log(msg.friendname);
		if(users[i].user==msg.friendname) {
			console.log(users[i].socket_id);
			socket.broadcast.to(users[i].socket_id).emit('videocall', {invitee:msg.invitee, room:msg.roomname});
		}
	  }	  	
  });

  socket.on('sendinvite', function(userdetails) {
	  console.log("invite got");
	  socket.join(userdetails.roomname);
	  console.log(userdetails.roomname);
	  socket.broadcast.to(userdetails.roomname).emit("message", "joined in room: "+userdetails.roomname);
	  for(i=0; i<users.length; i++) {
		if(users[i].user==userdetails.friendname) {
			socket.broadcast.to(users[i].socket_id).emit('invitation', {invitee:userdetails.user,room:userdetails.roomname});
		}
	  }	  	
  });

  socket.on('joinroom', function(room) {
	  try
	  {		  
		  socket.join(room,function() {
			  for(j=0;j<io.sockets.sockets.length;j++) {	
				 socket.broadcast.to(io.sockets.sockets[j].id).emit("joinedchat","joined");  
			  }
		  });
	  }
	  catch (e)
	  {
		  console.log(e.message);
	  }
  });
	
});