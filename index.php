<!DOCTYPE html>
<html lang="en">


<head>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.5/angular.min.js"></script>
	<script src="http://52.27.66.44:2014/socket.io/socket.io.js"></script>

	<script src="getMediaElement.js"></script>
	<script src="//cdn.webrtc-experiment.com/firebase.js"> </script>
	<script src="//cdn.webrtc-experiment.com/RTCPeerConnection-v1.5.js"> </script>
	<script src="//cdn.webrtc-experiment.com/video-conferencing/conference.js"> </script>

<style>
.contarea {
	float:left;
	width: 30%;
	border: 1px solid #DADADA;
	border-radius: 4px;
	height: 40%;
	overflow: auto;
}

.contarea ul {
	list-style: none;
	text-align: left;	
	margin: 0px;
	padding: 0px;
}

.contarea ul li {
	padding: 10px;
	border: 1x solid #CCCCCC;
	background: rgb(231, 231, 243);
	color: #000000;
	margin: 1px;
}

.contarea ul li:hover {
	cursor: pointer;
}

.messages {
	float: left;
	border: 1px solid #DADADA;
	border-radius: 4px;
	min-width: 50%;
	height: 400px;
	margin-top: 0px;
	overflow: auto;
	margin-left: 10px;
}

.messarea {
	clear: both;
	border: 0px solid #DADADA;
	border-radius: 4px;
	width: 55%;
	height: 70px;
	margin-left: 10px;
	margin-left: 31%;
}

.button {
	width: 100px;
	text-align: center;
	background: rgb(185, 185, 237);
	border: 1px solid #DADADA;
	border-radius: 3px;
	box-shadow: 1px 1px 1px 1px;
	padding: 3px;
	margin: 3px;
	color: #FFFFFF;
}

.button:hover {
	color: blue;
	background: #FFFFFF;
}

.messages {
	list-style: none;
}

.my-msg {
	color: #DADADA;
	width: 200px;	
	margin-right: 10px;
}

.friend-msg {
	width: 200px;
	color: #4FD5F9;
	font-weight: bold;
	margin-right: 10px;
}

.cam-overlay {
	background: #2D2D2D;
	opacity: 0.8;
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	position: absolute;
	top:0;
	left: 0;
	z-index: 999;
	display: none;
}

.cam-block {
	background: #FFFFFF;
	border-radius: 10px;
	position: absolute;
	z-index: 9999;
	width: 700px;
	height: 400px;
	top: 20%;
	left: 20%;
}

.text {
	padding: 3px;
	border: 1px solid #CCCCCC;
}

#welcome {
	display: none;
}
#chat_wrapper {
	opacity: 0.8;
	display: block;
	position: absolute;
	margin:0;
	padding:0;
	width: 100%;
	height: 100%;
}

#close-cam-block {
  position: relative;
  /* margin: 0; */
  /* clear: both; */
  width: 40px;
  height: 40px;
  background: url(close-2.png);
  background-repeat: no-repeat;
  float: right;
  display: block;
  z-index: 999999999;
}

.video-style {
	margin: 20px;
	padding: 20px;
	width: 200px!important;
}

.help {
	background: #F9F9FF;
	border-radius: 4px;
	width: 300px;
	display: none;
}
</style>
</head>

<body ng-app="myapp">


<div ng-controller="HelloController" >
	<!-- heading -->
    <h2>{{helloTo.title}}</h2>
	<div id="logindiv">
		<input type="text" id="sel-name" class="text"><input type="button" value="Login" id="login" class="button"> <span style="font-size: 11px;">(Please select any name from below list which is not of green background)</span>
	</div>
	<div class="help">
		 <span style="font-size: 11px;">(Wait for any user to come online. Once any user come online his background will be turn to green.)</span><br>
		 <span style="font-size: 11px;">(To Start chat with any user, please select user who is online.)</span>
	</div>
	<div id="welcome"><h1>Welcome <span id="welcome-username"></span></div>
	<div class="chat_area">
		<div id="chat_wrapper"></div>
		<!-- contacts area --> 
		<div class="contarea">
			<!-- friends -->
			<div ng-init="friends = [
			  {name:'John', age:25, gender:'boy'},
			  {name:'Jessie', age:30, gender:'girl'},
			  {name:'Johanna', age:28, gender:'girl'},
			  {name:'Joy', age:15, gender:'girl'},
			  {name:'Mary', age:28, gender:'girl'},
			  {name:'Peter', age:95, gender:'boy'},
			  {name:'Sebastian', age:50, gender:'boy'},
			  {name:'Erika', age:27, gender:'girl'},
			  {name:'Patrick', age:40, gender:'boy'},
			  {name:'Samantha', age:60, gender:'girl'}
			]">
			  <ul class="example-animate-container">
				<li id="fr-{{friend.name}}" class="animate-repeat" ng-repeat="friend in friends">
				  {{friend.name}}
				</li>
				<li class="animate-repeat" ng-if="results.length == 0">
				  <strong>No results found...</strong>
				</li>
			  </ul>
			</div>
		</div>
		<!-- messages area -->
		<ul class="messages"></ul>
		<div class="messarea">
	
			<div><textarea id="message" style="width: 97%; height: 10%; border-radius: 2px; margin-left: 2px;"></textarea></div>
			<div>
				<input type="button" value="Send" name="send" id="send" class="button" disabled="disabled">
				<input type="button" id="cam" value="Start Cam" class="button" disabled="disabled">
			</div>
		</div>
	</div> <!-- chat area -->

<!-- cam -->

<div class="cam-overlay">
	<div class="cam-block">
		<div id="close-cam-block"></div>
	</div>
</div>

<script>

	var room;
    var owner;
	var username ;
	var friend;
	var channel;
	var conferenceUI;
	angular.module("myapp", [])
    .controller("HelloController", function($scope) {
        $scope.helloTo = {};
        $scope.helloTo.title = "VChat";
    } );

	var videosContainer = $(".cam-block");

	var SIGNALING_SERVER = 'http://52.27.66.44:2014/';

	  var socket = io(SIGNALING_SERVER);

	// app methods
	$("document").ready(function() {
		getLoginUser();

		$("#close-cam-block").click(function() {
			$(".cam-overlay").hide();
		});

		$("#login").click(function() {
			var ok = 0;
			if($.trim($("#sel-name").val())!="") {
				$(".animate-repeat").each(function() {
					if($.trim($(this).text()) == $.trim($("#sel-name").val())) {
						ok++;
					}
				});
				if(ok>0) {
					$.ajax({
						url: 'api.php?action=login_user&name='+$.trim($("#sel-name").val()),
						type: 'GET',
						success: function(s) {
							var json = JSON.parse(s);
							if(json[0]=="success") {
								username = $.trim($("#sel-name").val());								
						     	socket.emit('addsocket',{user: username});
								$("#welcome-username").text(username);
								$("#welcome").show();
								$(".animate-repeat").each(function() {
									if($.trim($(this).text()) == username) {
										$(this).remove();
										$("#logindiv").remove();
										$("#chat_wrapper").css("display","none");
										$(".help").css("display","block");
									}
								});
							}
						},
						error: function(e) {
							console.log("Error: "+e.responseText);
						}
					});
				} else {
					alert("Please select User from List below. It is case-sensitive.");
				}
			}
		});

		$(".animate-repeat").click(function() {
			$(".animate-repeat").each(function() {
				$(this).css("background-color","rgb(231, 231, 243)");
				$(this).css("color","#000000");
			});
			$(this).css("background-color","rgb(87, 87, 148)");
			$(this).css("color","#FFFFFF");
			friend = $.trim($(this).text());
			room = "room-"+username+"-"+friend;
			channel = room;
			socket.emit('sendinvite',{roomname: room, user: username, friendname: friend});
			enablebuttons();
		});

		 $('#send').click(function(){
			if($.trim($('#message').val())!="") {
				$('.messages').append($('<li>').html("<span class='my-msg'>" + username+"</span>: "+$('#message').val()));
				socket.emit('chatmessage', {roomname: room, message: $('#message').val(), user: username});
				$('#message').val('');
				return false;
			}
		  });

		  $("#cam").click(function() {
			  channel = room;
			  conferenceUI = conference(config);
			  $(".cam-overlay").show();
			  captureUserMedia(function() {
					conferenceUI.createRoom({
						roomName: channel
					});
				}, function() {
				});
		  });
	});

	// enable buttons
	 function enablebuttons() {
		console.log("FGGhgfhgf");
		$("#send").attr("disabled",false);
		$("#cam").attr("disabled",false);
	 }

	

	// get login user
	function getLoginUser() {
		$.ajax({
			url: 'api.php?action=get_loggedin_user',
			type: 'GET',
			success: function(s) {
				var users = $.parseJSON(s);
				if(users.length>0) {
					$(".animate-repeat").each(function() {
						if($.inArray($.trim($(this).text()),users)>=0) {
							markUserOnline($(this));
						}
					});
				}
			},
			error: function(e) {
				console.log("Error: "+e.responseText);
			}
		});
	}

	// mark user online
	function markUserOnline(u) {
		$(u).css("background","rgb(225, 253, 225)");
	}

	// make user online
	function makeUserOnline(u) {
		$(".animate-repeat").each(function() {
			if($.trim($(this).text()) == u) {
				markUserOnline($(this));
			}
		});
	}

	// connect to chat
	 function connectChat(b) {
			friend = b.invitee;
			room = b.room;
			$(".animate-repeat").each(function() {
				$(this).css("background-color","rgb(231, 231, 243)");
				$(this).css("color","#000000");
				if($.trim($(this).text())==$.trim(b.invitee)) {
					$(this).css("background-color","rgb(87, 87, 148)");
					$(this).css("color","#FFFFFF");
					return false;
				}
			});
			socket.emit('joinroom',room);
	 }
		  
	
	  socket.on('connect', function() {
		  console.log("you are connected");
	  });

	  socket.on('newuser', function(u) {
		  console.log("new user "+u);
		  makeUserOnline(u);
	  });
		
	  socket.on('chatmessage', function(msg){
		  console.log("chat mssage: "+msg);
		$('.messages').append($('<li>').html("<span class='friend-msg'>" + msg.username + "</span>: " + msg.message));
	  });

	  socket.on('invitation', function(msg){
		  connectChat(msg);
		  enablebuttons();
	  });

	  socket.on('videocall',function(msg) {
		  recieveCall(msg);
	  });

	  socket.on('message', function(msg){	
		console.log(msg);
	  });

	  socket.on('takeoffer', function(msg) {
		getCall();
	  });

	  socket.on('created', function(room){
		owner = true;
		$('.messages').append($('<li>').text(room+ " created"));
		makeCall();
	  });

	  socket.on('joinedchat', function(joined){
		  console.log("ddfdf");
		  enablebuttons();
	  });

	  socket.on('full', function(room){
		$('.messages').append($('<li>').text(room+ " full"));
	  });

	  function recieveCall(msg) {
			channel = msg.room;
			console.log("Video call from: "+msg.invitee);
			conferenceUI = conference(config);
			$(".cam-overlay").show();
	  }
	

	var config = {
		openSocket: function(config) {			
			var socket = new Firebase('https://shining-fire-7540.firebaseio.com/' + channel);
			socket.channel = channel;
			socket.on("child_added", function(data) {
				config.onmessage && config.onmessage(data.val());
			});
			socket.send = function(data) {
				this.push(data);
			};
			config.onopen && setTimeout(config.onopen, 1);
			socket.onDisconnect().remove();
			return socket;
		},
		onRemoteStream: function(media) {
			console.log("---------stream found-----------");
			var mediaElement = getMediaElement(media.video, {
				width: 200,
				buttons: ['mute-audio', 'mute-video', 'full-screen', 'volume-slider']
			});
			mediaElement.id = media.streamid;
			if(videosContainer.find("video").length<2) {
				$(mediaElement).addClass("video-style");
				videosContainer.append(mediaElement);
			}
		},
		onRemoteStreamEnded: function(stream, video) {
			if (video.parentNode && video.parentNode.parentNode && video.parentNode.parentNode.parentNode) {
				video.parentNode.parentNode.parentNode.removeChild(video.parentNode.parentNode);
			}
		},
		onRoomFound: function(room) {
			console.log("--------room found-------");
			var exists = false;
			if(exists==false) {
				captureUserMedia(function() {
					conferenceUI.joinRoom({
						roomToken: room.roomToken,
						joinUser: room.broadcaster
					});
				}, function() {
					exists = true;
				});
			}
		},
		onRoomClosed: function(room) {
		}
	};
	

	

	function captureUserMedia(callback, failure_callback) {
		var video = document.createElement("video");
		getUserMedia({
			video: video,
			onsuccess: function(stream) {
				config.attachStream = stream;
				callback && callback();
				socket.emit();
				video.setAttribute('muted', true);
				console.log(friend);
				socket.emit('inviteVideoCall',{roomname: room, invitee: username, friendname: friend});
				var mediaElement = getMediaElement(video, {
					width: 200,
					buttons: ['mute-audio', 'mute-video', 'full-screen', 'volume-slider']
				});
				mediaElement.toggle('mute-audio');
				
				if(videosContainer.find("video").length<2) {
					$(mediaElement).addClass("video-style");
					videosContainer.append(mediaElement);
				}
			},
			onerror: function() {
				alert('unable to get access to your webcam');
				callback && callback();
			}
		});
	}

	function rotateVideo(video) {
		video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
		setTimeout(function() {
			video.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
		}, 1000);
	}

	(function() {
		var uniqueToken = document.getElementById('unique-token');
		if (uniqueToken)
			if (location.hash.length > 2) uniqueToken.parentNode.parentNode.parentNode.innerHTML = '<h2 style="text-align:center;"><a href="' + location.href + '" target="_blank">Share this link</a></h2>';
			else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace( /\./g , '-');
	})();

	function scaleVideos() {
		var videos = document.querySelectorAll('video'),
			length = videos.length, video;

		var minus = 130;
		var windowHeight = 700;
		var windowWidth = 600;
		var windowAspectRatio = windowWidth / windowHeight;
		var videoAspectRatio = 4 / 3;
		var blockAspectRatio;
		var tempVideoWidth = 0;
		var maxVideoWidth = 0;

		for (var i = length; i > 0; i--) {
			blockAspectRatio = i * videoAspectRatio / Math.ceil(length / i);
			if (blockAspectRatio <= windowAspectRatio) {
				tempVideoWidth = videoAspectRatio * windowHeight / Math.ceil(length / i);
			} else {
				tempVideoWidth = windowWidth / i;
			}
			if (tempVideoWidth > maxVideoWidth)
				maxVideoWidth = tempVideoWidth;
		}
		for (var i = 0; i < length; i++) {
			video = videos[i];
			if (video)
				video.width = maxVideoWidth - minus;
		}
	}

	window.onresize = scaleVideos;
	
</script><!--
<script src="app.js"></script>
-->
<!--
<script src="mycamchat.js"></script>
-->
</body>

</html>