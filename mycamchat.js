var browser = GetBrowser();
var video = document.getElementById("myvideo");
var remote_video = document.getElementById("friendvideo");
var streamToAttach;
var peerConnection;

function makeCall() {
	getStream();
}

function getStream() {
	if(browser=="chrome") {
		navigator.webkitGetUserMedia({ audio: true, video: true }, function (stream) {
			video.src = URL.createObjectURL(stream);
			streamToAttach = stream;
			createPeerConnection();
		}, function(error) {
			console.log(error);
		});
	} else {
		navigator.mozGetUserMedia({ audio: true, video: true }, function (stream) {
			video[0].mozSrcObject = stream;
			video[0].play();
			streamToAttach = stream;
			createPeerConnection();
		}, function(error) {
			console.log(error);
		});
	}
}

function createPeerConnection() {
	if(browser=="chrome") {
		peerConnection = new webkitRTCPeerConnection(
			{ "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] }
		);
	} else {
		peerConnection = new mozRTCPeerConnection(
			{ "iceServers": [{ "url": "stun:stun.services.mozilla.com" }] }
		);
	}	

	peerConnection.onicecandidate = onicecandidate;
	peerConnection.onaddstream = onaddstream;

	if(owner==true) {
		createOffer();
	} else {

	}

}	


function createOffer() {
	peerConnection.createOffer(function (sessionDescription) {
		peerConnection.setLocalDescription(sessionDescription, function() {
			socket.emit('offer', "accept offer");
		});
	}, function(error) {
		alert(error);
	}, { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } });
}

function getCall() {
	getStream();
	peerConnection.setRemoteDescription(new RTCSessionDescription(offer));	
	peerConnection.createAnswer(function (sessionDescription) {
		peerConnection.setLocalDescription(sessionDescription);
	}, function(error) {
		alert(error);
	}, { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } });
}



peerConnection.createAnswer(function (sessionDescription) {
    peerConnection.setLocalDescription(sessionDescription);
    
    // POST-answer-SDP-back-to-Offerer(sessionDescription.sdp, sessionDescription.type);

}, function(error) {
    alert(error);
}, { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } });


function onicecandidate(event) {
    if (!peerConnection || !event || !event.candidate) return;
    var candidate = event.candidate;
    // POST-ICE-to-other-Peer(candidate.candidate, candidate.sdpMLineIndex);

	peerConnection.addIceCandidate(new RTCIceCandidate({
		sdpMLineIndex: candidate.sdpMLineIndex,
		candidate: candidate.candidate
	}));

}

function onaddstream(event) {
    if (!event) return;
    remote_video.src = URL.createObjectURL(event.stream);
    // remote_video.mozSrcObject  = event.stream;
    
    waitUntilRemoteStreamStartsFlowing();
}

function GetBrowser()
{
    var n =  navigator ? navigator.userAgent.toLowerCase() : "other";
	if(n.indexOf("firefox")>=0) {
		return "firefox";
	} else if (n.indexOf("chrome")>=0)
	{
		return "chrome";
	}
}

function waitUntilRemoteStreamStartsFlowing()
{
    if (!(remote_video.readyState <= HTMLMediaElement.HAVE_CURRENT_DATA 
        || remote_video.paused || remote_video.currentTime <= 0)) 
    {
        // remote stream started flowing!
    } 
    else setTimeout(waitUntilRemoteStreamStartsFlowing, 50);
}

function getCamInChrome() {
	navigator.webkitGetUserMedia({ audio: true, video: true }, function (stream) {
		video.src = URL.createObjectURL(stream);
		peerConnection.addStream (stream);
	}, function(error) {
		console.log(error);
	});
}

function getCamInFirefox() {
	
}